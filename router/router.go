package router

import (
	"net/http"

	"github.com/go-chi/chi/v5"

	"gitlab.com/golight/example-service/modules/quotes/handlers"
)

func NewRouter(qh *handlers.QuotesHandler) *chi.Mux {
	router := chi.NewRouter()

	router.Get("/random", qh.GetRandomQuoteHandler)
	router.Get("/check", qh.CheckHandler)

	router.Get("/swagger", swaggerUI)
	router.Get("/static/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))).ServeHTTP(w, r)
	})

	return router
}
