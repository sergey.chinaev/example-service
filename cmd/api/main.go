package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"fmt"

	"gitlab.com/golight/example-service/router"

	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"gitlab.com/golight/cache"
	"gitlab.com/golight/dao/connector"
	conf "gitlab.com/golight/example-service/config"

	"gitlab.com/golight/example-service/modules/quotes/entity"
	"gitlab.com/golight/example-service/modules/quotes/handlers"
	qcache "gitlab.com/golight/example-service/modules/quotes/repository/cache"
	"gitlab.com/golight/example-service/modules/quotes/service"
	"gitlab.com/golight/example-service/modules/quotes/storage"
	"gitlab.com/golight/loggerx"
	"gitlab.com/golight/migrator"
	"gitlab.com/golight/scanner"
	"go.uber.org/zap"
)

func main() {
	appConf, err := conf.NewAppConfig()
	if err != nil {
		log.Fatal(err)
	}

	ts := scanner.NewTableScanner()

	ts.RegisterTable(&entity.Quote{})

	logger := loggerx.InitLogger(appConf.Name, appConf.Production)
	var sqlDB *connector.SqlDB
	sqlDB, err = connector.NewSqlDB(appConf.Db, ts, logger)
	if err != nil {
		logger.Fatal("failed to connect to db", zap.Error(err))
	}

	err = migrator.NewMigrator(sqlDB.DB, appConf.Db, ts).Migrate()
	if err != nil {
		logger.Fatal("failed to migrate", zap.Error(err))
	}

	qs := storage.NewQuotesStorage(sqlDB.DAO)

	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})

	cacheClient, err := cache.NewCache(appConf.Cache, decoder)

	if err != nil {
		fmt.Println(err)
	}

	qs = qcache.NewQuotesCache(qs, cacheClient)
	quotesrv := service.NewQuotes(qs, logger, "")

	err = quotesrv.Init(context.Background())
	if err != nil {
		logger.Fatal("failed to init quotes service", zap.Error(err))
	}

	qh := handlers.NewQuotesHandler(quotesrv)
	r := router.NewRouter(qh)

	server := http.Server{
		Addr:         ":4080",
		Handler:      r,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		log.Println("Starting server...")
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("Server error: %v", err)
		}
	}()

	<-sigChan

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err = server.Shutdown(ctx)
	if err != nil {
		log.Fatalf("Server shutdown error: %v", err)
	}

	log.Println("Server stopped gracefully")
}
