package main

import (
	"context"
	"fmt"
	"log"
	"net"

	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"gitlab.com/golight/cache"
	"gitlab.com/golight/dao/connector"
	conf "gitlab.com/golight/example-service/config"
	"gitlab.com/golight/example-service/modules/quotes/entity"
	"gitlab.com/golight/example-service/modules/quotes/qrpc/pb"
	"gitlab.com/golight/example-service/modules/quotes/qrpc/server"
	qcache "gitlab.com/golight/example-service/modules/quotes/repository/cache"
	"gitlab.com/golight/example-service/modules/quotes/storage"
	"gitlab.com/golight/example-service/modules/quotes/service"
	"gitlab.com/golight/loggerx"
	"gitlab.com/golight/migrator"
	"gitlab.com/golight/scanner"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

func main() {
	appConf, err := conf.NewAppConfig()
	if err != nil {
		log.Fatal(err)
	}

	ts := scanner.NewTableScanner()

	ts.RegisterTable(&entity.Quote{})

	logger := loggerx.InitLogger(appConf.Name, appConf.Production)
	var sqlDB *connector.SqlDB
	sqlDB, err = connector.NewSqlDB(appConf.Db, ts, logger)
	if err != nil {
		logger.Fatal("failed to connect to db", zap.Error(err))
	}

	err = migrator.NewMigrator(sqlDB.DB, appConf.Db, ts).Migrate()
	if err != nil {
		logger.Fatal("failed to migrate", zap.Error(err))
	}

	qs := storage.NewQuotesStorage(sqlDB.DAO)

	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})

	cacheClient, err := cache.NewCache(appConf.Cache, decoder)

	if err != nil {
		fmt.Println(err)
		return
	}

	qs = qcache.NewQuotesCache(qs, cacheClient)
	quotesrv := service.NewQuotes(qs, logger, "")

	err = quotesrv.Init(context.Background())
	if err != nil {
		logger.Fatal("failed to init quotes service", zap.Error(err))
	}

	s := InitRPC(quotesrv)
	lis, err := net.Listen("tcp", ":4080")
	if err != nil {
		logger.Error("failed to listen:", zap.Error(err))
	}

	logger.Info("grpc server listening at", zap.Stringer("address", lis.Addr()))
	if err = s.Serve(lis); err != nil {
		logger.Fatal("failed to serve:", zap.Error(err))
	}
}

func InitRPC(quoteser service.Quoteser) *grpc.Server {
	s := grpc.NewServer()
	pb.RegisterQuotesActionsRPCServer(s, server.NewQuotesRPCServer(quoteser))

	return s
}
