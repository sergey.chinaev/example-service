## Example-service
Сервис цитат

## Локальный запуск
1. Запустить скрипт "./dev.sh"
2. Запустить клиента "go run ./cmd/rpcclient/client.go" или с помощью IDE

## Документация экосистемы golight

# Boilerplate

Шаблонный код экосистемы golight
[See documentation in a README file](https://gitlab.com/golight/boilerplate)

# Cache

Пакет cache предоставляет абстракцию для работы с кэшем, которая позволяет легко заменять и мокать реализации кэша в приложении.
[See documentation in a README file](https://gitlab.com/golight/cache)

# DAO

Data Access Object экосистемы golight
[See documentation in a README file](https://gitlab.com/golight/dao)

# Entity

Entity экосистемы golight
[See documentation in a README file](https://gitlab.com/golight/entity)

# gocovci

Пакет gocovci роверяет покрытие тестами
[See documentation in a README file](https://gitlab.com/golight/gocovci)

# Gopubsub

Пакет Gopubsub представляет собой реализацию очереди сообщений с использованием Kafka Rabbit
[See documentation in a README file](https://gitlab.com/golight/gopubsub)

# Logger

Пакет Logger - это простой и настраиваемый логгер, построенный на базе библиотеки логирования Zap. Он предоставляет возможность инициализации логгера с настраиваемым режимом продакшн.
[See documentation in a README file](https://gitlab.com/golight/loggerx)

# Migrator

Migrator экосистемы golight
[See documentation in a README file](https://gitlab.com/golight/migrator)

# Orm

Пакет Orm автоматически генерирует файлы в репозитории
[See documentation in a README file](https://gitlab.com/golight/orm)

# Responder

Responder экосистемы golight
[See documentation in a README file](https://gitlab.com/golight/responder)

# Scanner

Пакет Scanner взаимодействует с таблицами экосистемы golight
[See documentation in a README file](https://gitlab.com/golight/scanner)

# Server

HTTP Server экосистемы golight
[See documentation in a README file](https://gitlab.com/golight/server)
