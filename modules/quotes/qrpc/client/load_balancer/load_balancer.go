package load_balancer

import (
	"context"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/golight/example-service/modules/quotes/qrpc/client"
)

// ClientStatus is a client status, which contains the client and the backoff time.
type ClientStatus struct {
	client    client.RPCQuoteser
	backoff   time.Duration
	nextCheck time.Time
}

// LoadBalancer is a load balancer interface, which contains the next method and the process clients method.
type LoadBalancer interface {
	Next() client.RPCQuoteser
	ProcessClients()
}

// QuotesLoadBalancer implements the LoadBalancer interface.
// inactiveClients - the inactive clients list of Quotes grpc clients.
// activeClients - the active clients list of Quotes grpc clients.
// processTick - the process tick, which is used for processing alive and dead clients.
// healthCheckTimeout - the health check timeout, which is used for checking the health of the clients.
// nextIndex - the next index for the next client, which is used for the round-robin algorithm.
// initialBackoff - the initial backoff time, which is used for the exponential backoff algorithm.
// maxBackoff - the maximum backoff time, which is used for the exponential backoff algorithm.
// mux - the mutex for the thread safety.
type QuotesLoadBalancer struct {
	inactiveClients    []*ClientStatus
	activeClients      []*ClientStatus
	processTick        time.Duration
	healthCheckTimeout time.Duration
	nextIndex          uint32
	initialBackoff     time.Duration
	maxBackoff         time.Duration
	mux                sync.Mutex
}

// LBOpt is a load balancer option.
type LBOpt func(balancer *QuotesLoadBalancer)

// NewLoadBalancer creates a new load balancer.
func NewLoadBalancer(clientsPool []client.RPCQuoteser, opts ...LBOpt) *QuotesLoadBalancer {
	clientStatuses := make([]*ClientStatus, len(clientsPool))
	for i, client := range clientsPool {
		clientStatuses[i] = &ClientStatus{
			client:  client,
			backoff: 10 * time.Minute,
		}
	}

	balancer := &QuotesLoadBalancer{
		activeClients:      clientStatuses,
		initialBackoff:     10 * time.Minute,
		maxBackoff:         6 * time.Hour,
		healthCheckTimeout: 500 * time.Millisecond,
		processTick:        1 * time.Second,
	}

	for _, opt := range opts {
		opt(balancer)
	}

	return balancer
}

// WithProcessTick functional option sets the process tick.
func WithProcessTick(tick time.Duration) LBOpt {
	return func(balancer *QuotesLoadBalancer) {
		balancer.processTick = tick
	}
}

// WithHealthCheckTimeout functional option sets the health check timeout.
func WithHealthCheckTimeout(timeout time.Duration) LBOpt {
	return func(balancer *QuotesLoadBalancer) {
		balancer.healthCheckTimeout = timeout
	}
}

// Next returns the next client, which is used for the round-robin algorithm.
func (r *QuotesLoadBalancer) Next() client.RPCQuoteser {
	var client client.RPCQuoteser
	r.mux.Lock()
	activeCount := len(r.activeClients)
	if activeCount == 0 {
		return nil
	}

	// Использование атомарного счетчика для выбора клиента
	client = r.activeClients[int(r.nextIndex%uint32(activeCount))].client
	r.mux.Unlock()
	atomic.AddUint32(&r.nextIndex, 1) // Инкремент для следующего вызова

	return client
}

// Eliminate eliminates the inactive clients, if they are unhealthy.
func (r *QuotesLoadBalancer) Eliminate() {
	var eliminatedClients []*ClientStatus
	var activeClients []*ClientStatus

	var err error
	for _, candidate := range r.activeClients {
		ctx, cancel := context.WithTimeout(context.Background(), r.healthCheckTimeout)
		if err = candidate.client.HealthCheck(ctx); err != nil {
			candidate.nextCheck = time.Now().Add(r.initialBackoff)
			candidate.backoff = r.initialBackoff
			eliminatedClients = append(eliminatedClients, candidate)
		} else {
			activeClients = append(activeClients, candidate)
		}
		cancel()
	}

	if len(eliminatedClients) < 1 {
		return
	}

	r.mux.Lock()
	r.activeClients = activeClients
	r.inactiveClients = append(r.inactiveClients, eliminatedClients...)
	r.mux.Unlock()
}

// Recover recovers the inactive clients, if they are healthy.
func (r *QuotesLoadBalancer) Recover() {
	var recoveredClients []*ClientStatus
	var inactiveClients []*ClientStatus

	for _, candidate := range r.inactiveClients {
		if time.Now().After(candidate.nextCheck) {
			ctx, cancel := context.WithTimeout(context.Background(), r.healthCheckTimeout)
			if err := candidate.client.HealthCheck(ctx); err == nil {
				recoveredClients = append(recoveredClients, candidate)
			} else {
				candidate.nextCheck = time.Now().Add(candidate.backoff)
				candidate.backoff *= 2
				if candidate.backoff > r.maxBackoff {
					candidate.backoff = r.maxBackoff
				}
				inactiveClients = append(inactiveClients, candidate)
			}
			cancel()
		}
	}

	if len(recoveredClients) < 1 {
		return
	}

	r.mux.Lock()
	r.activeClients = append(r.activeClients, recoveredClients...)
	r.inactiveClients = inactiveClients
	r.mux.Unlock()
}

// ProcessClients processes the clients.
func (r *QuotesLoadBalancer) ProcessClients() {
	go func() {
		r.Eliminate()
		for range time.Tick(r.processTick) {
			r.Eliminate()
		}
	}()
	go func() {
		for range time.Tick(r.processTick) {
			r.Recover()
		}
	}()
}
