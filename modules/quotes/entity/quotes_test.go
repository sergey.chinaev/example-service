package entity

import (
	"reflect"
	"testing"
)

func TestQuoteTableName(t *testing.T) {
	q := &Quote{}
	expectedTableName := "quotes"
	result := q.TableName()

	if result != expectedTableName {
		t.Errorf("Expected table name %s, but got %s", expectedTableName, result)
	}
}

func TestQuoteOnCreate(t *testing.T) {
	q := &Quote{}
	expectedOnCreate := []string{}
	result := q.OnCreate()

	if !reflect.DeepEqual(result, expectedOnCreate) {
		t.Errorf("Expected OnCreate result %v, but got %v", expectedOnCreate, result)
	}
}

func TestQuoteFieldsPointers(t *testing.T) {
	q := &Quote{}
	expectedFieldsPointers := []interface{}{
		&q.ID,
		&q.Text,
		&q.Author,
		&q.CreatedAt,
		&q.UpdatedAt,
		&q.DeletedAt,
	}
	result := q.FieldsPointers()

	if !reflect.DeepEqual(result, expectedFieldsPointers) {
		t.Errorf("Expected FieldsPointers result %v, but got %v", expectedFieldsPointers, result)
	}
}
