package handlers

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/golang/mock/gomock"
	"gitlab.com/golight/example-service/modules/quotes/entity"
	qmock "gitlab.com/golight/example-service/modules/quotes/handlers/mocks"
)

func TestQuotesHandler_GetRandomQuote(t *testing.T) {
	var quote = entity.QuoteDTO{
		ID:     123,
		Text:   "test_text",
		Author: "test_author",
	}

	tests := []struct {
		name       string
		prepare    func(quoteser *qmock.MockQuoteser)
		wantStatus int
		wantErr    bool
	}{
		{
			name: "GetRandomQuote",
			prepare: func(quoteserMock *qmock.MockQuoteser) {
				quoteserMock.EXPECT().GetRandomQuote(gomock.Any()).
					Times(1).
					Return(quote, nil)
			},
			wantStatus: http.StatusOK,
		},
		{
			name: "GetRandomQuote rpcQuotes.GetRandomQuote err",
			prepare: func(quoteserMock *qmock.MockQuoteser) {
				quoteserMock.EXPECT().GetRandomQuote(gomock.Any()).
					Times(1).
					Return(quote, errors.New("test"))
			},
			wantStatus: http.StatusInternalServerError,
			wantErr:    true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteserMock := qmock.NewMockQuoteser(gomock.NewController(t))
			tt.prepare(quoteserMock)
			h := NewQuotesHandler(quoteserMock)

			recorder := httptest.NewRecorder()
			req := httptest.NewRequest("GET", "/random", nil)
			h.GetRandomQuoteHandler(recorder, req)

			result := recorder.Result()

			if result.StatusCode != tt.wantStatus {
				t.Errorf("TestQuotesHandler status got: %d, want %d", result.StatusCode, tt.wantStatus)
			}

			if !tt.wantErr {
				// Проверка, что тело ответа содержит ожидаемую цитату
				var gotQuote entity.QuoteDTO
				err := json.NewDecoder(result.Body).Decode(&gotQuote)
				if err != nil {
					t.Errorf("Failed to decode JSON response body: %v", err)
				}

				if !reflect.DeepEqual(gotQuote, quote) {
					t.Error("TestQuotesHandler got body is not equals want")
				}
			}
		})
	}
}

func TestQuotesHandler_CheckHandler(t *testing.T) {
	tests := []struct {
		name       string
		prepare    func(quoteserMock *qmock.MockQuoteser)
		wantStatus int
		wantBody   string
		wantErr    bool
	}{
		{
			name: "CheckHandler success",
			prepare: func(quoteserMock *qmock.MockQuoteser) {
				quoteserMock.EXPECT().Check(gomock.Any()).
					Times(1).
					Return(nil)
			},
			wantStatus: http.StatusOK,
			wantBody:   "Service is available",
		},
		{
			name: "CheckHandler quotesService.Check error",
			prepare: func(quoteserMock *qmock.MockQuoteser) {
				quoteserMock.EXPECT().Check(gomock.Any()).
					Times(1).
					Return(errors.New("test"))
			},
			wantStatus: http.StatusInternalServerError,
			wantErr:    true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteserMock := qmock.NewMockQuoteser(gomock.NewController(t))
			tt.prepare(quoteserMock)
			h := NewQuotesHandler(quoteserMock)

			recorder := httptest.NewRecorder()
			req := httptest.NewRequest("GET", "/check", nil)
			h.CheckHandler(recorder, req)

			result := recorder.Result()

			if result.StatusCode != tt.wantStatus {
				t.Errorf("TestQuotesHandler status got: %d, want %d", result.StatusCode, tt.wantStatus)
			}

			if !tt.wantErr {
				gotBody, _ := io.ReadAll(result.Body)
				if string(gotBody) != tt.wantBody {
					t.Errorf("TestQuotesHandler got body is not equals want: got %s, want %s", gotBody, tt.wantBody)
				}
			}
		})
	}
}
