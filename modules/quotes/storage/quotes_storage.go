package storage

import (
	"context"

	"gitlab.com/golight/dao"
	"gitlab.com/golight/dao/errors"
	"gitlab.com/golight/dao/params"
	"gitlab.com/golight/entity/tabler"
	"gitlab.com/golight/example-service/modules/quotes/entity"
	"gitlab.com/golight/example-service/modules/quotes/repository"
)

const (
	QuotesCount = 1617
)

// QuotesStorage авто сгенерированный репозиторий цитат
type QuotesStorage struct {
	adapter dao.DAOFace
}

// NewQuotesStorage конструктор репозитория цитат
func NewQuotesStorage(sqlAdapter dao.DAOFace) repository.Quoteser {
	return &QuotesStorage{adapter: sqlAdapter}
}

// Ping проверка доступности репозитория, с помощью проверки доступности базы данных
func (q *QuotesStorage) Ping(ctx context.Context) error {
	return q.adapter.Ping(ctx)
}

// Count метод получения количества цитат
func (q *QuotesStorage) Count(ctx context.Context) (uint64, error) {
	return q.adapter.GetCount(ctx, &entity.Quote{}, params.Condition{})
}

// Create метод создания цитаты
func (q *QuotesStorage) Create(ctx context.Context, dto entity.Quote) error {
	return q.adapter.Create(ctx, &dto)
}

// GetByID метод получения цитаты по идентификатору
func (q *QuotesStorage) GetByID(ctx context.Context, quotesID int) (entity.Quote, error) {
	var list []entity.Quote
	var table entity.Quote
	err := q.adapter.List(ctx, &list, &table, params.Condition{
		Equal: map[string]interface{}{"id": quotesID},
	})
	if err != nil {
		return entity.Quote{}, err
	}
	if len(list) < 1 {
		return entity.Quote{}, &errors.NotFound{}
	}

	return list[0], err
}

// Upsert метод обновления цитаты
func (q *QuotesStorage) Upsert(ctx context.Context, entities []*entity.Quote, opts ...interface{}) error {
	ents := make([]tabler.Tabler, len(entities))
	for i, v := range entities {
		ents[i] = v
	}

	return q.adapter.Upsert(ctx, ents, opts...)
}

// InitData метод инициализации данных
// Если в таблице нет данных, то вставляет переданные данные,
// используется блокировка таблицы в режиме SHARE ROW EXCLUSIVE (блокировка чтения и записи)
func (q *QuotesStorage) InitData(ctx context.Context, entities []*entity.Quote) (bool, error) {
	tx, err := q.adapter.Begin()
	if err != nil {
		return false, err
	}
	defer func() {
		if err != nil {
			_ = tx.Rollback()
		}
	}()

	err = q.adapter.LockTable(ctx, "quotes", "SHARE ROW EXCLUSIVE", tx)
	if err != nil {
		return false, err
	}

	count, err := q.adapter.GetCount(ctx, &entity.Quote{}, params.Condition{}, tx)
	if err != nil {
		return false, err
	}
	if count > 0 {
		return false, tx.Commit()
	}

	err = q.Upsert(ctx, entities, tx)
	if err != nil {
		return false, err
	}

	return true, tx.Commit()
}

// GetList метод получения списка цитат
func (q *QuotesStorage) GetList(ctx context.Context) ([]*entity.Quote, error) {
	var list []*entity.Quote
	var table entity.Quote
	err := q.adapter.List(ctx, &list, &table, params.Condition{})
	if err != nil {
		return nil, err
	}

	return list, nil
}
