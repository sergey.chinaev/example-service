package storage

import (
	"context"
	"errors"
	"reflect"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/golight/dao/mock"
	"gitlab.com/golight/dao/params"
	"gitlab.com/golight/entity/tabler"
	"gitlab.com/golight/example-service/modules/quotes/entity"
)

func TestQuotesStorage_Count(t *testing.T) {
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		args    args
		want    uint64
		prepare func(daoMock *mock.MockDAOFace)
		wantErr bool
	}{
		{
			name: "count",
			args: args{ctx: context.Background()},
			want: uint64(123),
			prepare: func(daoMock *mock.MockDAOFace) {
				daoMock.EXPECT().GetCount(gomock.Any(), gomock.AssignableToTypeOf(&entity.Quote{}),
					gomock.Eq(params.Condition{})).
					Times(1).
					Return(uint64(123), nil)
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			daoMock := mock.NewMockDAOFace(gomock.NewController(t))
			tt.prepare(daoMock)
			q := NewQuotesStorage(daoMock)

			got, err := q.Count(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("Count() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Count() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestQuotesStorage_Create(t *testing.T) {
	type args struct {
		ctx context.Context
		dto entity.Quote
	}
	tests := []struct {
		name    string
		args    args
		prepare func(daoMock *mock.MockDAOFace)
		wantErr bool
	}{
		{
			name: "create",
			args: args{ctx: context.Background()},
			prepare: func(daoMock *mock.MockDAOFace) {
				daoMock.EXPECT().Create(gomock.Any(), gomock.AssignableToTypeOf(&entity.Quote{})).
					Times(1).
					Return(nil)
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			daoMock := mock.NewMockDAOFace(gomock.NewController(t))
			tt.prepare(daoMock)
			q := NewQuotesStorage(daoMock)

			if err := q.Create(tt.args.ctx, tt.args.dto); (err != nil) != tt.wantErr {
				t.Errorf("Create() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestQuotesStorage_GetByID(t *testing.T) {
	type args struct {
		ctx      context.Context
		quotesID int
	}
	tests := []struct {
		name    string
		args    args
		want    entity.Quote
		prepare func(daoMock *mock.MockDAOFace)
		wantErr bool
	}{
		{
			name: "get quote by id",
			args: args{
				ctx:      context.Background(),
				quotesID: 1,
			},
			want: entity.Quote{
				ID: 1,
			},
			prepare: func(daoMock *mock.MockDAOFace) {
				daoMock.EXPECT().List(gomock.Any(), gomock.AssignableToTypeOf(&[]entity.Quote{}),
					gomock.AssignableToTypeOf(&entity.Quote{}), gomock.AssignableToTypeOf(params.Condition{})).
					SetArg(1, []entity.Quote{{ID: 1}}).
					Times(1).
					Return(nil)
			},
			wantErr: false,
		},
		{
			name: "adapter get err",
			args: args{ctx: context.Background()},
			prepare: func(daoMock *mock.MockDAOFace) {
				daoMock.EXPECT().List(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
					Times(1).
					Return(errors.New("test"))
			},
			wantErr: true,
		},
		{
			name: "adapter get quotes",
			args: args{ctx: context.Background()},
			prepare: func(daoMock *mock.MockDAOFace) {
				daoMock.EXPECT().List(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
					SetArg(1, []entity.Quote{}).
					Times(1).
					Return(nil)
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			daoMock := mock.NewMockDAOFace(gomock.NewController(t))
			tt.prepare(daoMock)
			q := NewQuotesStorage(daoMock)

			got, err := q.GetByID(tt.args.ctx, tt.args.quotesID)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got.ID != tt.want.ID {
				t.Errorf("GetByID() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestQuotesStorage_GetList(t *testing.T) {
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		args    args
		want    []*entity.Quote
		prepare func(daoMock *mock.MockDAOFace)
		wantErr bool
	}{
		{
			name: "get list",
			args: args{ctx: context.Background()},
			want: []*entity.Quote{{ID: 1}, {ID: 2}},
			prepare: func(daoMock *mock.MockDAOFace) {
				daoMock.EXPECT().List(gomock.Any(), gomock.AssignableToTypeOf(&[]*entity.Quote{}),
					gomock.AssignableToTypeOf(&entity.Quote{}), gomock.Eq(params.Condition{})).
					SetArg(1, []*entity.Quote{{ID: 1}, {ID: 2}}).
					Times(1).
					Return(nil)
			},
			wantErr: false,
		},
		{
			name: "adapter get err",
			args: args{ctx: context.Background()},
			prepare: func(daoMock *mock.MockDAOFace) {
				daoMock.EXPECT().List(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).
					Times(1).
					Return(errors.New("test"))
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			daoMock := mock.NewMockDAOFace(gomock.NewController(t))
			tt.prepare(daoMock)
			q := NewQuotesStorage(daoMock)

			got, err := q.GetList(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetList() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetList() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestQuotesStorage_InitData(t *testing.T) {
	type args struct {
		ctx      context.Context
		entities []*entity.Quote
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		prepare func(daoMock *mock.MockDAOFace)
		wantErr bool
	}{
		{
			name: "init data true",
			args: args{
				ctx:      context.Background(),
				entities: []*entity.Quote{{}, {}, {}},
			},
			want: true,
			prepare: func(daoMock *mock.MockDAOFace) {
				db, err := sqlx.Open("sqlite3", ":memory:")
				if err != nil {
					t.Fatal(err)
				}
				defer db.Close()
				tx, err := db.Beginx()
				if err != nil {
					t.Fatal(err)
				}

				daoMock.EXPECT().Begin().Times(1).Return(tx, nil)
				daoMock.EXPECT().LockTable(gomock.Any(), gomock.Eq("quotes"),
					gomock.Eq("SHARE ROW EXCLUSIVE"), gomock.Eq(tx)).
					Times(1).
					Return(nil)
				daoMock.EXPECT().GetCount(gomock.Any(), gomock.AssignableToTypeOf(&entity.Quote{}),
					gomock.Eq(params.Condition{}), gomock.Eq(tx)).
					Times(1).
					Return(uint64(0), nil)
				daoMock.EXPECT().Upsert(gomock.Any(), gomock.Eq([]tabler.Tabler{
					&entity.Quote{}, &entity.Quote{}, &entity.Quote{},
				}), gomock.Eq([]interface{}{tx})).
					Times(1).
					Return(nil)
			},
			wantErr: false,
		},
		{
			name: "init data false",
			args: args{
				ctx:      context.Background(),
				entities: []*entity.Quote{{}, {}, {}},
			},
			want: false,
			prepare: func(daoMock *mock.MockDAOFace) {
				db, err := sqlx.Open("sqlite3", ":memory:")
				if err != nil {
					t.Fatal(err)
				}
				defer db.Close()
				tx, err := db.Beginx()
				if err != nil {
					t.Fatal(err)
				}

				daoMock.EXPECT().Begin().Times(1).Return(tx, nil)
				daoMock.EXPECT().LockTable(gomock.Any(), gomock.Eq("quotes"),
					gomock.Eq("SHARE ROW EXCLUSIVE"), gomock.Eq(tx)).
					Times(1).
					Return(nil)
				daoMock.EXPECT().GetCount(gomock.Any(), gomock.AssignableToTypeOf(&entity.Quote{}),
					gomock.Eq(params.Condition{}), gomock.Eq(tx)).
					Times(1).
					Return(uint64(1), nil)
			},
			wantErr: false,
		},
		{
			name: "adapter.Begin err",
			args: args{
				ctx:      context.Background(),
				entities: []*entity.Quote{{}, {}, {}},
			},
			prepare: func(daoMock *mock.MockDAOFace) {
				daoMock.EXPECT().Begin().Times(1).Return(nil, errors.New("test"))
			},
			wantErr: true,
		},
		{
			name: "adapter.LockTable err",
			args: args{
				ctx:      context.Background(),
				entities: []*entity.Quote{{}, {}, {}},
			},
			prepare: func(daoMock *mock.MockDAOFace) {
				db, err := sqlx.Open("sqlite3", ":memory:")
				if err != nil {
					t.Fatal(err)
				}
				defer db.Close()
				tx, err := db.Beginx()
				if err != nil {
					t.Fatal(err)
				}

				daoMock.EXPECT().Begin().Times(1).Return(tx, nil)
				daoMock.EXPECT().LockTable(gomock.Any(), gomock.Eq("quotes"),
					gomock.Eq("SHARE ROW EXCLUSIVE"), gomock.Eq(tx)).
					Times(1).
					Return(errors.New("test"))
			},
			wantErr: true,
		},
		{
			name: "adapter.GetCount err",
			args: args{
				ctx:      context.Background(),
				entities: []*entity.Quote{{}, {}, {}},
			},
			prepare: func(daoMock *mock.MockDAOFace) {
				db, err := sqlx.Open("sqlite3", ":memory:")
				if err != nil {
					t.Fatal(err)
				}
				defer db.Close()
				tx, err := db.Beginx()
				if err != nil {
					t.Fatal(err)
				}

				daoMock.EXPECT().Begin().Times(1).Return(tx, nil)
				daoMock.EXPECT().LockTable(gomock.Any(), gomock.Eq("quotes"),
					gomock.Eq("SHARE ROW EXCLUSIVE"), gomock.Eq(tx)).
					Times(1).
					Return(nil)
				daoMock.EXPECT().GetCount(gomock.Any(), gomock.AssignableToTypeOf(&entity.Quote{}),
					gomock.Eq(params.Condition{}), gomock.Eq(tx)).
					Times(1).
					Return(uint64(0), errors.New("test"))
			},
			wantErr: true,
		},
		{
			name: "Upsert err",
			args: args{
				ctx:      context.Background(),
				entities: []*entity.Quote{{}, {}, {}},
			},
			prepare: func(daoMock *mock.MockDAOFace) {
				db, err := sqlx.Open("sqlite3", ":memory:")
				if err != nil {
					t.Fatal(err)
				}
				defer db.Close()
				tx, err := db.Beginx()
				if err != nil {
					t.Fatal(err)
				}

				daoMock.EXPECT().Begin().Times(1).Return(tx, nil)
				daoMock.EXPECT().LockTable(gomock.Any(), gomock.Eq("quotes"),
					gomock.Eq("SHARE ROW EXCLUSIVE"), gomock.Eq(tx)).
					Times(1).
					Return(nil)
				daoMock.EXPECT().GetCount(gomock.Any(), gomock.AssignableToTypeOf(&entity.Quote{}),
					gomock.Eq(params.Condition{}), gomock.Eq(tx)).
					Times(1).
					Return(uint64(0), nil)
				daoMock.EXPECT().Upsert(gomock.Any(), gomock.Eq([]tabler.Tabler{
					&entity.Quote{}, &entity.Quote{}, &entity.Quote{},
				}), gomock.Eq([]interface{}{tx})).
					Times(1).
					Return(errors.New("test"))
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			daoMock := mock.NewMockDAOFace(gomock.NewController(t))
			tt.prepare(daoMock)
			q := NewQuotesStorage(daoMock)

			got, err := q.InitData(tt.args.ctx, tt.args.entities)
			if (err != nil) != tt.wantErr {
				t.Errorf("InitData() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("InitData() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestQuotesStorage_Ping(t *testing.T) {
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		args    args
		prepare func(daoMock *mock.MockDAOFace)
		wantErr bool
	}{
		{
			name: "ping",
			args: args{ctx: context.Background()},
			prepare: func(daoMock *mock.MockDAOFace) {
				daoMock.EXPECT().Ping(gomock.Any()).
					Times(1).
					Return(nil)
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			daoMock := mock.NewMockDAOFace(gomock.NewController(t))
			tt.prepare(daoMock)
			q := NewQuotesStorage(daoMock)

			if err := q.Ping(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("Ping() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestQuotesStorage_Upsert(t *testing.T) {
	type args struct {
		ctx      context.Context
		entities []*entity.Quote
		opts     []interface{}
	}
	tests := []struct {
		name    string
		args    args
		prepare func(daoMock *mock.MockDAOFace)
		wantErr bool
	}{
		{
			name: "upsert",
			args: args{
				ctx:      context.Background(),
				entities: []*entity.Quote{{}, {}, {}},
				opts:     []interface{}{},
			},
			prepare: func(daoMock *mock.MockDAOFace) {
				daoMock.EXPECT().Upsert(gomock.Any(), gomock.Eq([]tabler.Tabler{
					&entity.Quote{}, &entity.Quote{}, &entity.Quote{},
				}),
					gomock.Eq([]interface{}{})).
					Times(1).
					Return(nil)
			},
			wantErr: false,
		},
		{
			name: "err",
			args: args{
				ctx:      context.Background(),
				entities: []*entity.Quote{{}, {}, {}},
				opts:     []interface{}{},
			},
			prepare: func(daoMock *mock.MockDAOFace) {
				daoMock.EXPECT().Upsert(gomock.Any(), gomock.Eq([]tabler.Tabler{
					&entity.Quote{}, &entity.Quote{}, &entity.Quote{},
				}), gomock.Eq([]interface{}{})).
					Times(1).
					Return(errors.New("test"))
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			daoMock := mock.NewMockDAOFace(gomock.NewController(t))
			tt.prepare(daoMock)
			q := NewQuotesStorage(daoMock)

			if err := q.Upsert(tt.args.ctx, tt.args.entities, tt.args.opts...); (err != nil) != tt.wantErr {
				t.Errorf("Upsert() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
