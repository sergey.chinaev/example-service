package cache

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/golight/example-service/modules/quotes/entity"
	mock "gitlab.com/golight/example-service/modules/quotes/repository/mocks"
	qmock "gitlab.com/golight/example-service/modules/quotes/service/mocks"
)

func TestQuotesCache_Count(t *testing.T) {
	type args struct {
		context context.Context
	}
	tests := []struct {
		name             string
		args             args
		prepareCacherGet func(cacher *mock.MockCacher)
		prepareCacherSet func(cacher *mock.MockCacher)
		prepareQuoteser  func(quoteser *qmock.MockQuoteser)
		want             uint64
		wantErr          bool
	}{
		{
			name: "StatusCode",
			args: args{
				context: context.Background(),
			},
			prepareCacherGet: func(cacher *mock.MockCacher) {
				var n uint64
				cacher.EXPECT().Get(gomock.Any(), "count_quotes", &n).
					Times(1).
					Return(errors.New("cache miss"))
			},
			prepareCacherSet: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "count_quotes", uint64(0), cacheTTL).
					Times(1).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Count(gomock.Any()).
					Times(1).
					Return(uint64(0), nil)
			},
			want:    uint64(0),
			wantErr: false,
		},
		{
			name: "From cache",
			args: args{
				context: context.Background(),
			},
			prepareCacherGet: func(cacher *mock.MockCacher) {
				var n uint64
				cacher.EXPECT().Get(gomock.Any(), "count_quotes", &n).
					Times(1).
					Return(nil)
			},
			prepareCacherSet: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "count_quotes", uint64(0), cacheTTL).
					Times(0).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Count(gomock.Any()).
					Times(0).
					Return(uint64(0), nil)
			},
			want:    uint64(0),
			wantErr: false,
		},
		{
			name: "Error q.storage.Count()",
			args: args{
				context: context.Background(),
			},
			prepareCacherGet: func(cacher *mock.MockCacher) {
				var n uint64
				cacher.EXPECT().Get(gomock.Any(), "count_quotes", &n).
					Times(1).
					Return(errors.New("cache miss"))
			},
			prepareCacherSet: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "count_quotes", uint64(0), cacheTTL).
					Times(0).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Count(gomock.Any()).
					Times(1).
					Return(uint64(0), errors.New("some error"))
			},
			want:    uint64(0),
			wantErr: true,
		},
		{
			name: "Error q.cache.Set()",
			args: args{
				context: context.Background(),
			},
			prepareCacherGet: func(cacher *mock.MockCacher) {
				var n uint64
				cacher.EXPECT().Get(gomock.Any(), "count_quotes", &n).
					Times(1).
					Return(errors.New("cache miss"))
			},
			prepareCacherSet: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "count_quotes", uint64(1), cacheTTL).
					Times(1).
					Return(errors.New("some error"))
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Count(gomock.Any()).
					Times(1).
					Return(uint64(1), nil)
			},
			want:    uint64(0),
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteser := qmock.NewMockQuoteser(gomock.NewController(t))
			cacher := mock.NewMockCacher(gomock.NewController(t))

			tt.prepareCacherGet(cacher)
			tt.prepareCacherSet(cacher)
			tt.prepareQuoteser(quoteser)

			quotesCache := NewQuotesCache(quoteser, cacher)

			count, err := quotesCache.Count(tt.args.context)
			if (err != nil) != tt.wantErr {
				t.Errorf("Count() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, count)
		})
	}
}

func TestQuotesCache_Create(t *testing.T) {
	type args struct {
		ctx context.Context
		dto entity.Quote
	}
	tests := []struct {
		name            string
		args            args
		prepareCacher   func(cacher *mock.MockCacher)
		prepareQuoteser func(quoteser *qmock.MockQuoteser)
		wantErr         bool
	}{
		{
			name: "StatusCode",
			args: args{
				ctx: context.Background(),
				dto: entity.Quote{},
			},
			prepareCacher: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), false, "count_quotes", "quotes_list").
					Times(1).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Create(gomock.Any(), gomock.AssignableToTypeOf(entity.Quote{})).
					Times(1).
					Return(nil)
			},
			wantErr: false,
		},
		{
			name: "Error q.storage.Create()",
			args: args{
				ctx: context.Background(),
				dto: entity.Quote{},
			},
			prepareCacher: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), false, "count_quotes", "quotes_list").
					Times(0).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Create(gomock.Any(), gomock.AssignableToTypeOf(entity.Quote{})).
					Times(1).
					Return(errors.New("some error"))
			},
			wantErr: true,
		},
		{
			name: "Error q.cache.Delete()",
			args: args{
				ctx: context.Background(),
				dto: entity.Quote{},
			},
			prepareCacher: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), false, "count_quotes", "quotes_list").
					Times(1).
					Return(errors.New("some error"))
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Create(gomock.Any(), gomock.AssignableToTypeOf(entity.Quote{})).
					Times(1).
					Return(nil)
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteser := qmock.NewMockQuoteser(gomock.NewController(t))
			cacher := mock.NewMockCacher(gomock.NewController(t))

			tt.prepareCacher(cacher)
			tt.prepareQuoteser(quoteser)

			quotesCache := NewQuotesCache(quoteser, cacher)

			err := quotesCache.Create(tt.args.ctx, tt.args.dto)
			if (err != nil) != tt.wantErr {
				t.Errorf("Create() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestQuotesCache_GetByID(t *testing.T) {
	type args struct {
		context  context.Context
		quotesID int
	}
	tests := []struct {
		name             string
		args             args
		prepareCacherGet func(cacher *mock.MockCacher)
		prepareCacherSet func(cacher *mock.MockCacher)
		prepareQuoteser  func(quoteser *qmock.MockQuoteser)
		want             entity.Quote
		wantErr          bool
	}{
		{
			name: "StatusCode",
			args: args{
				context:  context.Background(),
				quotesID: 1,
			},
			prepareCacherGet: func(cacher *mock.MockCacher) {
				var quote entity.Quote
				cacher.EXPECT().Get(gomock.Any(), "quotes:1", &quote).
					Times(1).
					Return(errors.New("cache miss"))
			},
			prepareCacherSet: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "quotes:1", entity.Quote{}, cacheTTL).
					Times(1).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().GetByID(gomock.Any(), 1).
					Times(1).
					Return(entity.Quote{}, nil)
			},
			want:    entity.Quote{},
			wantErr: false,
		},
		{
			name: "Error q.storage.GetByID()",
			args: args{
				context:  context.Background(),
				quotesID: 1,
			},
			prepareCacherGet: func(cacher *mock.MockCacher) {
				var quote entity.Quote
				cacher.EXPECT().Get(gomock.Any(), "quotes:1", &quote).
					Times(1).
					Return(errors.New("cache miss"))
			},
			prepareCacherSet: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "quotes:1", entity.Quote{}, cacheTTL).
					Times(0).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().GetByID(gomock.Any(), 1).
					Times(1).
					Return(entity.Quote{}, errors.New("some error"))
			},
			want:    entity.Quote{},
			wantErr: true,
		},
		{
			name: "From cache",
			args: args{
				context:  context.Background(),
				quotesID: 1,
			},
			prepareCacherGet: func(cacher *mock.MockCacher) {
				var quote entity.Quote
				cacher.EXPECT().Get(gomock.Any(), "quotes:1", &quote).
					Times(1).
					Return(nil)
			},
			prepareCacherSet: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "quotes:1", entity.Quote{}, cacheTTL).
					Times(0).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().GetByID(gomock.Any(), 1).
					Times(0).
					Return(entity.Quote{}, errors.New("some error"))
			},
			want:    entity.Quote{},
			wantErr: false,
		},
		{
			name: "Error q.cache.Set()",
			args: args{
				context:  context.Background(),
				quotesID: 1,
			},
			prepareCacherGet: func(cacher *mock.MockCacher) {
				var quote entity.Quote
				cacher.EXPECT().Get(gomock.Any(), "quotes:1", &quote).
					Times(1).
					Return(errors.New("some error"))
			},
			prepareCacherSet: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "quotes:1", entity.Quote{}, cacheTTL).
					Times(1).
					Return(errors.New("some error"))
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().GetByID(gomock.Any(), 1).
					Times(1).
					Return(entity.Quote{}, nil)
			},
			want:    entity.Quote{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteser := qmock.NewMockQuoteser(gomock.NewController(t))
			cacher := mock.NewMockCacher(gomock.NewController(t))

			tt.prepareCacherGet(cacher)
			tt.prepareCacherSet(cacher)
			tt.prepareQuoteser(quoteser)

			quotesCache := NewQuotesCache(quoteser, cacher)

			res, err := quotesCache.GetByID(tt.args.context, tt.args.quotesID)
			if (err != nil) != tt.wantErr {
				t.Errorf("Count() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, res)
		})
	}
}

func TestQuotesCache_GetList(t *testing.T) {
	type args struct {
		context context.Context
	}
	tests := []struct {
		name             string
		args             args
		prepareCacherGet func(cacher *mock.MockCacher)
		prepareCacherSet func(cacher *mock.MockCacher)
		prepareQuoteser  func(quoteser *qmock.MockQuoteser)
		want             []*entity.Quote
		wantErr          bool
	}{
		{
			name: "StatusCode",
			args: args{
				context: context.Background(),
			},
			prepareCacherGet: func(cacher *mock.MockCacher) {
				var quotes []*entity.Quote
				cacher.EXPECT().Get(gomock.Any(), "quotes_list", &quotes).
					Times(1).
					Return(errors.New("cache miss"))
			},
			prepareCacherSet: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "quotes_list", []*entity.Quote{}, cacheTTL).
					Times(1).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().GetList(gomock.Any()).
					Times(1).
					Return([]*entity.Quote{}, nil)
			},
			want:    []*entity.Quote{},
			wantErr: false,
		},
		{
			name: "From cache",
			args: args{
				context: context.Background(),
			},
			prepareCacherGet: func(cacher *mock.MockCacher) {
				var quotes []*entity.Quote
				cacher.EXPECT().Get(gomock.Any(), "quotes_list", &quotes).
					Times(1).
					Return(nil)
			},
			prepareCacherSet: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "quotes_list", []*entity.Quote{}, cacheTTL).
					Times(0).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().GetList(gomock.Any()).
					Times(0).
					Return([]*entity.Quote{}, nil)
			},
			want:    []*entity.Quote(nil),
			wantErr: false,
		},
		{
			name: "Error q.storage.GetList()",
			args: args{
				context: context.Background(),
			},
			prepareCacherGet: func(cacher *mock.MockCacher) {
				var quotes []*entity.Quote
				cacher.EXPECT().Get(gomock.Any(), "quotes_list", &quotes).
					Times(1).
					Return(errors.New("cache miss"))
			},
			prepareCacherSet: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "quotes_list", []*entity.Quote{}, cacheTTL).
					Times(0).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().GetList(gomock.Any()).
					Times(1).
					Return([]*entity.Quote{}, errors.New("some error"))
			},
			want:    []*entity.Quote(nil),
			wantErr: true,
		},
		{
			name: "Error q.cache.Set()",
			args: args{
				context: context.Background(),
			},
			prepareCacherGet: func(cacher *mock.MockCacher) {
				var quotes []*entity.Quote
				cacher.EXPECT().Get(gomock.Any(), "quotes_list", &quotes).
					Times(1).
					Return(errors.New("cache miss"))
			},
			prepareCacherSet: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Set(gomock.Any(), "quotes_list", []*entity.Quote{}, cacheTTL).
					Times(1).
					Return(errors.New("some error"))
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().GetList(gomock.Any()).
					Times(1).
					Return([]*entity.Quote{}, nil)
			},
			want:    []*entity.Quote(nil),
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteser := qmock.NewMockQuoteser(gomock.NewController(t))
			cacher := mock.NewMockCacher(gomock.NewController(t))

			tt.prepareCacherGet(cacher)
			tt.prepareCacherSet(cacher)
			tt.prepareQuoteser(quoteser)

			quotesCache := NewQuotesCache(quoteser, cacher)

			res, err := quotesCache.GetList(tt.args.context)
			if (err != nil) != tt.wantErr {
				t.Errorf("Count() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, res)
		})
	}
}

func TestQuotesCache_InitData(t *testing.T) {
	type args struct {
		context  context.Context
		entities []*entity.Quote
	}
	tests := []struct {
		name                      string
		args                      args
		prepareCacherDeleteFirst  func(cacher *mock.MockCacher)
		prepareCacherDeleteSecond func(cacher *mock.MockCacher)
		prepareQuoteser           func(quoteser *qmock.MockQuoteser)
		want                      bool
		wantErr                   bool
	}{
		{
			name: "StatusCode",
			args: args{
				context:  context.Background(),
				entities: []*entity.Quote{},
			},
			prepareCacherDeleteFirst: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), true, "quotes:*").
					Times(1).
					Return(nil)
			},
			prepareCacherDeleteSecond: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), false, "count_quotes", "quotes_list").
					Times(1).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().InitData(gomock.Any(), []*entity.Quote{}).
					Times(1).
					Return(true, nil)
			},
			want:    true,
			wantErr: false,
		},
		{
			name: "Error q.storage.InitData()",
			args: args{
				context:  context.Background(),
				entities: []*entity.Quote{},
			},
			prepareCacherDeleteFirst: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), true, "quotes:*").
					Times(0).
					Return(nil)
			},
			prepareCacherDeleteSecond: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), false, "count_quotes", "quotes_list").
					Times(0).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().InitData(gomock.Any(), []*entity.Quote{}).
					Times(1).
					Return(false, errors.New("some error"))
			},
			want:    false,
			wantErr: true,
		},
		{
			name: "Error q.cache.Delete() first time",
			args: args{
				context:  context.Background(),
				entities: []*entity.Quote{},
			},
			prepareCacherDeleteFirst: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), true, "quotes:*").
					Times(1).
					Return(errors.New("some error"))
			},
			prepareCacherDeleteSecond: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), false, "count_quotes", "quotes_list").
					Times(0).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().InitData(gomock.Any(), []*entity.Quote{}).
					Times(1).
					Return(true, nil)
			},
			want:    true,
			wantErr: true,
		},
		{
			name: "Error q.cache.Delete() second time",
			args: args{
				context:  context.Background(),
				entities: []*entity.Quote{},
			},
			prepareCacherDeleteFirst: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), true, "quotes:*").
					Times(1).
					Return(nil)
			},
			prepareCacherDeleteSecond: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), false, "count_quotes", "quotes_list").
					Times(1).
					Return(errors.New("some error"))
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().InitData(gomock.Any(), []*entity.Quote{}).
					Times(1).
					Return(true, nil)
			},
			want:    true,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteser := qmock.NewMockQuoteser(gomock.NewController(t))
			cacher := mock.NewMockCacher(gomock.NewController(t))

			tt.prepareCacherDeleteFirst(cacher)
			tt.prepareCacherDeleteSecond(cacher)
			tt.prepareQuoteser(quoteser)

			quotesCache := NewQuotesCache(quoteser, cacher)

			res, err := quotesCache.InitData(tt.args.context, tt.args.entities)
			if (err != nil) != tt.wantErr {
				t.Errorf("Count() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, res)
		})
	}
}

func TestQuotesCache_Ping(t *testing.T) {
	type args struct {
		context context.Context
	}
	tests := []struct {
		name            string
		args            args
		prepareQuoteser func(quoteser *qmock.MockQuoteser)
		want            bool
		wantErr         bool
	}{
		{
			name: "StatusCode",
			args: args{context: context.Background()},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Ping(gomock.Any()).
					Times(1).
					Return(nil)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteser := qmock.NewMockQuoteser(gomock.NewController(t))
			cacher := mock.NewMockCacher(gomock.NewController(t))

			tt.prepareQuoteser(quoteser)

			quotesCache := NewQuotesCache(quoteser, cacher)

			err := quotesCache.Ping(tt.args.context)
			if (err != nil) != tt.wantErr {
				t.Errorf("Count() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestQuotesCache_Upsert(t *testing.T) {
	type args struct {
		context  context.Context
		entities []*entity.Quote
	}
	tests := []struct {
		name                      string
		args                      args
		prepareCacherDeleteFirst  func(cacher *mock.MockCacher)
		prepareCacherDeleteSecond func(cacher *mock.MockCacher)
		prepareQuoteser           func(quoteser *qmock.MockQuoteser)
		wantErr                   bool
	}{
		{
			name: "StatusCode",
			args: args{
				context:  context.Background(),
				entities: []*entity.Quote{},
			},
			prepareCacherDeleteFirst: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), true, "quotes:*").
					Times(1).
					Return(nil)
			},
			prepareCacherDeleteSecond: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), false, "count_quotes", "quotes_list").
					Times(1).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Upsert(gomock.Any(), []*entity.Quote{}).
					Times(1).
					Return(nil)
			},
			wantErr: false,
		},
		{
			name: "Error q.storage.Upsert()",
			args: args{
				context:  context.Background(),
				entities: []*entity.Quote{},
			},
			prepareCacherDeleteFirst: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), true, "quotes:*").
					Times(0).
					Return(nil)
			},
			prepareCacherDeleteSecond: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), false, "count_quotes", "quotes_list").
					Times(0).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Upsert(gomock.Any(), []*entity.Quote{}).
					Times(1).
					Return(errors.New("some error"))
			},
			wantErr: true,
		},
		{
			name: "Error q.cache.Delete() first time",
			args: args{
				context:  context.Background(),
				entities: []*entity.Quote{},
			},
			prepareCacherDeleteFirst: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), true, "quotes:*").
					Times(1).
					Return(errors.New("some error"))
			},
			prepareCacherDeleteSecond: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), false, "count_quotes", "quotes_list").
					Times(0).
					Return(nil)
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Upsert(gomock.Any(), []*entity.Quote{}).
					Times(1).
					Return(nil)
			},
			wantErr: true,
		},
		{
			name: "Error q.cache.Delete() second time",
			args: args{
				context:  context.Background(),
				entities: []*entity.Quote{},
			},
			prepareCacherDeleteFirst: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), true, "quotes:*").
					Times(1).
					Return(nil)
			},
			prepareCacherDeleteSecond: func(cacher *mock.MockCacher) {
				cacher.EXPECT().Delete(gomock.Any(), false, "count_quotes", "quotes_list").
					Times(1).
					Return(errors.New("some error"))
			},
			prepareQuoteser: func(quoteser *qmock.MockQuoteser) {
				quoteser.EXPECT().Upsert(gomock.Any(), []*entity.Quote{}).
					Times(1).
					Return(nil)
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteser := qmock.NewMockQuoteser(gomock.NewController(t))
			cacher := mock.NewMockCacher(gomock.NewController(t))

			tt.prepareCacherDeleteFirst(cacher)
			tt.prepareCacherDeleteSecond(cacher)
			tt.prepareQuoteser(quoteser)

			quotesCache := NewQuotesCache(quoteser, cacher)

			err := quotesCache.Upsert(tt.args.context, tt.args.entities)
			if (err != nil) != tt.wantErr {
				t.Errorf("Count() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
