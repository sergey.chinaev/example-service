package service

import (
	"context"
	"encoding/json"
	"errors"
	"math/rand"
	"os"
	"reflect"
	"sync"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	derrors "gitlab.com/golight/dao/errors"
	"gitlab.com/golight/example-service/modules/quotes/entity"
	"gitlab.com/golight/example-service/modules/quotes/storage"
	qmock "gitlab.com/golight/example-service/modules/quotes/service/mocks"
	"gitlab.com/golight/loggerx"
)

func ReadQuoteFromFile(path string) ([]*entity.Quote, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	var quote []*entity.Quote
	err = json.Unmarshal(data, &quote)
	if err != nil {
		return nil, err
	}

	return quote, nil
}

func TestReadQuoteFromBinary(t *testing.T) {
	testData, err := ReadQuoteFromFile("../../../static/quotes.json")
	if err != nil {
		t.Error(err)
	}
	type args struct {
		path string
	}
	tests := []struct {
		name    string
		args    args
		want    []*entity.Quote
		prepare func(args *args)
		wantErr bool
	}{
		{
			name:    "Read exist file",
			args:    args{path: "../../../static/quotes.dat"},
			want:    testData,
			prepare: func(args *args) {},
			wantErr: false,
		},
		{
			name: "Error os.Open",
			args: args{},
			prepare: func(args *args) {
				args.path = t.TempDir() + "/test_file"
				f, err := os.Create(args.path)
				if err != nil {
					t.Error(err)
				}
				defer f.Close()
				if err := f.Chmod(0111); err != nil {
					t.Error(err)
				}
			},
			wantErr: true,
		},
		{
			name: "Error decoder.Decode",
			args: args{},
			prepare: func(args *args) {
				args.path = t.TempDir() + "/test_file"
				f, err := os.Create(args.path)
				if err != nil {
					t.Error(err)
				}
				defer f.Close()
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.prepare(&tt.args)
			got, err := ReadQuoteFromBinary(tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("ReadQuoteFromBinary() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(got) != len(tt.want) {
				t.Errorf("ReadQuoteFromBinary() got = %v, want %v", got, tt.want)
			}
			if !Equality(got, tt.want) {
				t.Errorf("ReadQuoteFromBinary() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Equality(first, second []*entity.Quote) bool {
	if len(first) != len(second) {
		return false
	}

	for i := range first {
		if first[i].ID != second[i].ID || first[i].Text != second[i].Text || first[i].Author != second[i].Author {
			return false
		}
	}

	return true
}

func BenchmarkReadQuoteFromBinary(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		_, _ = ReadQuoteFromBinary("static/quote.dat")
	}
}

func BenchmarkReadQuoteFromFile(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		_, _ = ReadQuoteFromFile("static/quote.json")
	}
}

func TestQuotes_Check(t *testing.T) {
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		args    args
		prepare func(quoteserMock *qmock.MockQuoteser)
		wantErr bool
	}{
		{
			name: "Check",
			args: args{ctx: context.Background()},
			prepare: func(quoteserMock *qmock.MockQuoteser) {
				quoteserMock.EXPECT().Ping(gomock.Any()).
					Times(1).
					Return(nil)
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteserMock := qmock.NewMockQuoteser(gomock.NewController(t))
			tt.prepare(quoteserMock)
			q := &Quotes{repo: quoteserMock}

			if err := q.Check(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("Check() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestQuotes_GetRandomQuote(t *testing.T) {
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		args    args
		want    entity.QuoteDTO
		prepare func(quoteserMock *qmock.MockQuoteser)
		wantErr bool
	}{
		{
			name: "GetRandomQuote",
			args: args{ctx: context.Background()},
			want: entity.QuoteDTO{
				ID:     1,
				Text:   "2",
				Author: "3",
			},
			prepare: func(quoteserMock *qmock.MockQuoteser) {
				quoteserMock.EXPECT().Count(gomock.Any()).
					Times(1).
					Return(uint64(1), nil)
				quoteserMock.EXPECT().
					GetByID(gomock.Any(), gomock.Any()).
					Times(1).
					Return(entity.Quote{
						ID:     1,
						Text:   "2",
						Author: "3",
					}, nil)
			},
			wantErr: false,
		},
		{
			name: "Error q.repo.Count",
			args: args{ctx: context.Background()},
			prepare: func(quoteserMock *qmock.MockQuoteser) {
				quoteserMock.EXPECT().Count(gomock.Any()).
					Times(1).
					Return(uint64(0), errors.New("test"))
			},
			wantErr: true,
		},
		{
			name: "Error q.repo.GetByID",
			args: args{ctx: context.Background()},
			prepare: func(quoteserMock *qmock.MockQuoteser) {
				quoteserMock.EXPECT().Count(gomock.Any()).
					Times(1).
					Return(uint64(1), nil)
				quoteserMock.EXPECT().
					GetByID(gomock.Any(), gomock.Any()).
					Times(1).
					Return(entity.Quote{}, errors.New("test"))
			},
			wantErr: true,
		},
		{
			name: "Wrong random id 0",
			args: args{ctx: context.Background()},
			prepare: func(quoteserMock *qmock.MockQuoteser) {
				quoteserMock.EXPECT().Count(gomock.Any()).
					Times(1).
					Return(uint64(1617), nil)
				quoteserMock.EXPECT().
					GetByID(gomock.Any(), gomock.Any()).
					Times(1).
					Return(entity.Quote{}, errors.New("test"))
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteserMock := qmock.NewMockQuoteser(gomock.NewController(t))
			tt.prepare(quoteserMock)
			var opts []QuotesOpt
			if tt.name == "Wrong random id 0" {
				r := R{
					value: -1,
				}
				opts = append(opts, WithRandomizer(r))
			}
			q := NewQuotes(quoteserMock,
				loggerx.InitLogger("test_name", false), "", opts...)

			got, err := q.GetRandomQuote(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetRandomQuote() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetRandomQuote() got = %v, want %v", got, tt.want)
			}
		})
	}
}

type R struct {
	value int
}

func (r R) Intn(int) int {
	return r.value
}

func TestQuotes_Init(t *testing.T) {
	type args struct {
		ctx      context.Context
		dataPath string
		wg       *sync.WaitGroup
	}
	tests := []struct {
		name    string
		args    args
		prepare func(quoteserMock *qmock.MockQuoteser, args args)
		wantErr bool
	}{
		{
			name: "init mutated=true",
			args: args{
				ctx:      context.Background(),
				dataPath: "../../../static/quotes.dat",
				wg:       &sync.WaitGroup{},
			},
			prepare: func(quoteserMock *qmock.MockQuoteser, args args) {
				quoteserMock.EXPECT().Count(gomock.Any()).
					Times(1).
					Return(uint64(0), nil)
				quoteserMock.EXPECT().
					InitData(gomock.Any(), gomock.AssignableToTypeOf([]*entity.Quote{})).
					Times(1).
					Return(true, nil)

				quotesFromGetList := make([]*entity.Quote, rand.Intn(1000)+1)
				for i := range quotesFromGetList {
					quotesFromGetList[i] = &entity.Quote{}
				}
				args.wg.Add(len(quotesFromGetList))
				quoteserMock.EXPECT().GetList(gomock.Any()).
					Times(1).
					Return(quotesFromGetList, nil)
				quoteserMock.EXPECT().
					GetByID(gomock.Any(), gomock.Eq(0)).
					Times(len(quotesFromGetList)).
					DoAndReturn(func(context.Context, int) (entity.Quote, error) {
						args.wg.Done()
						return entity.Quote{}, nil
					})
			},
			wantErr: false,
		},
		{
			name: "count > 0",
			args: args{ctx: context.Background()},
			prepare: func(quoteserMock *qmock.MockQuoteser, args args) {
				quoteserMock.EXPECT().Count(gomock.Any()).
					Times(1).
					Return(uint64(1), nil)
			},
			wantErr: false,
		},
		{
			name: "Error q.repo.Count",
			args: args{ctx: context.Background()},
			prepare: func(quoteserMock *qmock.MockQuoteser, args args) {
				quoteserMock.EXPECT().Count(gomock.Any()).
					Times(1).
					Return(uint64(0), errors.New("test"))
			},
			wantErr: true,
		},
		{
			name: "Error q.ReadQuoteFromBinary",
			args: args{
				ctx:      context.Background(),
				dataPath: "value for err",
			},
			prepare: func(quoteserMock *qmock.MockQuoteser, args args) {
				quoteserMock.EXPECT().Count(gomock.Any()).
					Times(1).
					Return(uint64(0), nil)
			},
			wantErr: true,
		},
		{
			name: "Error q.repo.CountInitData",
			args: args{
				ctx:      context.Background(),
				dataPath: "../../../static/quotes.dat",
			},
			prepare: func(quoteserMock *qmock.MockQuoteser, args args) {
				quoteserMock.EXPECT().Count(gomock.Any()).
					Times(1).
					Return(uint64(0), nil)
				quoteserMock.EXPECT().
					InitData(gomock.Any(), gomock.AssignableToTypeOf([]*entity.Quote{})).
					Times(1).
					Return(false, errors.New("test"))
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteserMock := qmock.NewMockQuoteser(gomock.NewController(t))
			tt.prepare(quoteserMock, tt.args)
			q := NewQuotes(quoteserMock,
				loggerx.InitLogger("test_name", false),
				tt.args.dataPath)

			if err := q.Init(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("Init() error = %v, wantErr %v", err, tt.wantErr)
			}

			if tt.args.wg != nil {
				c := make(chan struct{})
				go func() {
					tt.args.wg.Wait()
					close(c)
				}()
				select {
				case <-time.NewTimer(time.Second * 5).C:
				case <-c:
				}
			}
		})
	}
}

func TestQuotes_checkNotFound(t *testing.T) {
	type args struct {
		ctx context.Context
		err error
		id  int
	}
	tests := []struct {
		name    string
		args    args
		want    entity.QuoteDTO
		prepare func(quoteserMock *qmock.MockQuoteser)
		wantErr bool
	}{
		{
			name: "GetRandomQuote",
			args: args{
				ctx: context.Background(),
				err: &derrors.NotFound{},
				id:  0,
			},
			want: entity.QuoteDTO{
				ID:     1,
				Text:   "2",
				Author: "3",
			},
			prepare: func(quoteserMock *qmock.MockQuoteser) {
				quotesFromGetList := make([]*entity.Quote, storage.QuotesCount+1)
				quotesFromGetList[0] = &entity.Quote{
					ID:     1,
					Text:   "2",
					Author: "3",
				}
				quoteserMock.EXPECT().GetList(gomock.Any()).
					Times(1).
					Return(quotesFromGetList, nil)
			},
			wantErr: false,
		},
		{
			name: "Error q.repo.GetList",
			args: args{
				ctx: context.Background(),
				err: &derrors.NotFound{},
			},
			prepare: func(quoteserMock *qmock.MockQuoteser) {
				quoteserMock.EXPECT().GetList(gomock.Any()).
					Times(1).
					Return(nil, errors.New("test"))
			},
			wantErr: true,
		},
		{
			name: "quotes < QuotesCount",
			args: args{
				ctx: context.Background(),
				err: &derrors.NotFound{},
				id:  0,
			},
			prepare: func(quoteserMock *qmock.MockQuoteser) {
				quotesFromGetList := make([]*entity.Quote, storage.QuotesCount-1)
				quoteserMock.EXPECT().GetList(gomock.Any()).
					Times(1).
					Return(quotesFromGetList, nil)
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteserMock := qmock.NewMockQuoteser(gomock.NewController(t))
			tt.prepare(quoteserMock)
			q := &Quotes{repo: quoteserMock}

			got, err := q.checkNotFound(tt.args.ctx, tt.args.err, tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("checkNotFound() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("checkNotFound() got = %v, want %v", got, tt.want)
			}
		})
	}
}
