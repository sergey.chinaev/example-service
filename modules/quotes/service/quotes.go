package service

import (
	"context"
	"encoding/gob"
	"math/rand"
	"os"
	"path/filepath"
	"time"

	derrors "gitlab.com/golight/dao/errors"
	"gitlab.com/golight/example-service/modules/quotes/entity"
	"gitlab.com/golight/example-service/modules/quotes/storage"
	"gitlab.com/golight/example-service/modules/quotes/repository"
	"go.uber.org/zap"
)

const (
	QuotesDataPath = "static/quotes.dat"
)

// Quoteser interface for Quotes service
//
//go:generate mockgen -source=./quotes.go -destination=./mocks/quotes_mock.go -package=mock
type Quoteser interface {
	GetRandomQuote(ctx context.Context) (entity.QuoteDTO, error)
	Check(ctx context.Context) error
	Init(ctx context.Context) error
}

// Quotes сервис цитат
type Quotes struct {
	dataPath string
	repo     repository.Quoteser
	r        Randomizer
	logger   *zap.Logger
}

// QuotesOpt опции для сервиса Quotes
type QuotesOpt func(*Quotes)

// WithRandomizer опция для установки рандомайзера
func WithRandomizer(r Randomizer) QuotesOpt {
	return func(q *Quotes) {
		q.r = r
	}
}

// NewQuotes конструктор сервиса цитат
func NewQuotes(repo repository.Quoteser, logger *zap.Logger, dataPath string, opts ...QuotesOpt) Quoteser {
	seed := time.Now().UnixNano()

	if dataPath == "" {
		dataPath = QuotesDataPath
	}

	q := &Quotes{
		repo:     repo,
		r:        rand.New(rand.NewSource(seed)),
		dataPath: dataPath,
		logger:   logger,
	}

	for _, opt := range opts {
		opt(q)
	}

	return q
}

// GetRandomQuote метод получения случайной цитаты
// checkNotFound - метод проверки на ошибку NotFound и возврат случайной цитаты (уже не используется) TODO: удалить
func (q *Quotes) GetRandomQuote(ctx context.Context) (entity.QuoteDTO, error) {
	count, err := q.repo.Count(ctx)
	if err != nil {
		return entity.QuoteDTO{}, err
	}
	// skip check overflow int
	id := q.r.Intn(int(count)) + 1
	if id == 0 {
		id = 1
	}
	quote, err := q.repo.GetByID(ctx, id)
	if err != nil {
		return q.checkNotFound(ctx, err, id-1)
	}

	return entity.QuoteDTO{
		ID:     quote.ID,
		Text:   quote.Text,
		Author: quote.Author,
	}, nil
}

// checkNotFound метод проверки на ошибку NotFound и возврат случайной цитаты
//
// Deprecated: метод больше не используется, поскольку при инициализации данных
// в базе данных, не происходит конфликта в Primary Key, id цитаты идут по порядку.
// Исправлено эксклюзивной блокировкой таблицы во время инициализации данных в методе Init
func (q *Quotes) checkNotFound(ctx context.Context, err error, id int) (entity.QuoteDTO, error) {
	var quote entity.QuoteDTO
	if _, ok := err.(*derrors.NotFound); ok {
		quotes, listErr := q.repo.GetList(ctx)
		if listErr != nil {
			return quote, listErr
		}
		if len(quotes) < storage.QuotesCount {
			return quote, err
		}
		if id > len(quotes) {
			id = rand.Intn(len(quotes))
		}
		quote.ID = quotes[id].ID
		quote.Text = quotes[id].Text
		quote.Author = quotes[id].Author

		return quote, nil
	}

	return quote, err
}

// Init метод инициализации данных
func (q *Quotes) Init(ctx context.Context) error {
	count, err := q.repo.Count(ctx)
	if err != nil {
		return err
	}

	if count > 0 {
		q.logger.Info("Quotes already initialized")
		return nil
	}

	quotes, err := ReadQuoteFromBinary(q.dataPath)
	if err != nil {
		return err
	}

	var mutated bool
	mutated, err = q.repo.InitData(ctx, quotes)
	if err != nil {
		return err
	}

	// Pre init cache
	if mutated {
		go func() {
			quotes, _ = q.repo.GetList(ctx)
			for _, quote := range quotes {
				_, _ = q.repo.GetByID(ctx, quote.ID)
			}
		}()
	}

	return nil
}

// Check метод проверки доступности сервиса
func (q *Quotes) Check(ctx context.Context) error {
	return q.repo.Ping(ctx)
}

// ReadQuoteFromBinary метод чтения цитат из файла бинарного формата
func ReadQuoteFromBinary(path string) ([]*entity.Quote, error) {
	// Убедитесь, что файл существует перед его открытием
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return nil, err
	}

	// Открытие файла для чтения
	file, err := os.Open(filepath.Clean(path))
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var quotes []*entity.Quote

	// Создание gob.Decoder и чтение данных непосредственно из файла
	decoder := gob.NewDecoder(file)
	err = decoder.Decode(&quotes)
	if err != nil {
		return nil, err
	}

	return quotes, nil
}
