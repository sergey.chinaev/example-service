package service

// Randomizer интерфейс для рандомайзера цитат, чтобы можно было использовать моки в тестах
type Randomizer interface {
	Intn(n int) int
}
