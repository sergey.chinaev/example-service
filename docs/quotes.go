package docs

import "gitlab.com/golight/example-service/modules/quotes/entity"

// swagger:route GET /random getRandomQuote
// responses:
//   200: Quote
//   500:
//     description: Internal Server Error

// swagger:response Quote
type Quote struct {
	// in:body
	Body entity.QuoteDTO
}


// swagger:route GET /check checkService
// responses:
//   200:
//     description: Service is available
//   500:
//     description: Internal Server Error